import { Router } from "express";
import mercadoCtrl from "../controllers/mercado.controller";

const { realizarPago } = mercadoCtrl;

const router = Router();

router.route("/").post(realizarPago);

export default router;
