import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import indexRouter from './routes/index';
import mercadopago from 'mercadopago.js'
import mercadoRouter from './routes/mercado.routes'

const app = express();

mercadopago.configurations.setAccessToken("TEST-4950074102407218-101620-188845128f802351ef994cc5ec4fd09d-191381109")

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.use('/', indexRouter);
app.use('/api/mercadopago', mercadoRouter)

export default app;
